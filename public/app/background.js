import { loggedIn } from './Common.js';
import {followBackFollowers} from './Followers.js';
import {shareCloset} from './Closet.js';

chrome.runtime.onMessage.addListener(
    function(request, sender, sendResponse) {
        switch(request.action) {
            case "running-follow-back-followers":
                let state = sessionStorage.getItem("running-follow-back-followers");
                sendResponse(state);
                return true;
            case "follow-back-followers":
                sessionStorage.setItem("running-follow-back-followers", "true");
                followBackFollowers().then(result => {
                        sessionStorage.setItem("running-follow-back-followers", "false");
                        sendResponse(result);
                });
                return true;
            case "is-logged-in":
                loggedIn().then(result => {
                    sendResponse(result);
                });
                return true;
            case "share-closet":
                sessionStorage.setItem("running-share-closet", "true");
                shareCloset().then(result => {
                    sessionStorage.setItem("running-share-closet", "false");
                    sendResponse(result);
                });
                return true;
            case "running-share-closet":
                let x = sessionStorage.getItem("running-share-closet");
                sendResponse(x);
                return true;
            default:
                return true;
            }
    }
);