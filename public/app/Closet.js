import { sleep, getUserIdOrNull } from './Common.js';

export async function shareCloset() {
    let userId = await getUserIdOrNull();
    let posts = await getPosts(userId);
    let postsFiltered = posts.filter(isPublishedAndAvailable);
    let postsFilteredSorted = postsFiltered.sort(comparePostStatusChanged);
    
    console.log("Closet - Sharing:")
    console.log(postsFilteredSorted);
    return sharePosts(postsFilteredSorted);
}

function isPublishedAndAvailable(post) {
    return post.status === "published" && post.inventory.status === "available";
}

function comparePostStatusChanged(a, b) {
    let aDate = new Date(a.status_changed_at);
    let bDate = new Date(b.status_changed_at);

    return aDate - bDate;
}

async function getPosts(userId, max_id=null) {

    function url() {
        if(max_id !== null) {
            return "https://poshmark.com/api/users/" + userId + "/posts?&max_id=" + max_id;
        } else {
            return "https://poshmark.com/api/users/" + userId + "/posts";
        }
    }

    let method = "GET";
    let response = await fetch(url(), {
        method: method
    });

    if(response.status === 200) {
        let json = await response.json();
        if(json.more !== undefined) {
            return json.data.concat(await getPosts(userId, json.more.next_max_id));
        } else {
            return json.data;
        }
    } else {
        console.log(response.statusText);
        throw new Error("Failed to get closet list. responseStatus: " + response.status + " responseText:" + response.statusText);
    }
}

async function sharePosts(list) {
    if(list.length > 0) {
        const [head, ...tail] = list;
        let post = await sharePost(head);
        console.log("Shared: " + post);
        return [post].concat(await sharePosts(tail));
    } else {
        return [];
    }
}

async function sharePost(post) {
    let url = "https://poshmark.com/listing/share?post_id=" + post.id
    let method = "POST";

    let response = await fetch(url, {
        method: method
    });

    let wait = Math.floor(1000.00 * (1.0 + Math.random()));

    await sleep(wait);

    if(response.status === 201 || response.status === 200) {
        let json = await response.json();
        console.log(json.data);
        return true;
    } else {
        console.log(response.statusText);
        throw new Error("Failed to share listing id: " + post.id + " . responseStatus: " + response.status + " responseText:" + response.statusText);

    }
}
