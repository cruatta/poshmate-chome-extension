/* global chrome */ 
import './chrome-extension-async.js'

export async function getUserIdOrNull() {
    let userCookie = await chrome.cookies.get({"url": "https://poshmark.com/", "name": "tatari-user-cookie"});
    if(userCookie === null) {
        return null;
    }
    return userCookie.value;
}

export async function loggedIn() {
    let x = await chrome.cookies.get({"url": "https://poshmark.com/", "name": "jwt"});
    if(x === null) {
        console.log("Logged in: false");
        return false;
    } else {
        console.log("Logged in: true");
        return true;
    }
}

export function sleep(ms) {
    return new Promise(resolve => setTimeout(resolve, ms));
}