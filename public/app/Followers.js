import { sleep, getUserIdOrNull } from './Common.js';

export async function followBackFollowers() {
    let userId = await getUserIdOrNull();
    let followers = await getFollowersList(userId);
    let following = await getFollowingList(userId);
    let diff = filterNotFollowing(followers, following);
    console.log("Followers - Going to follow:")
    console.log(diff);

    return followUserList(diff);
}

async function followUserList(list) {
    if(list.length > 0) {
        const [head, ...tail] = list;
        let user = await followUser(head);
        console.log("Followers - Following: " + user);
        return [user].concat(await followUserList(tail));
    } else {
        return [];
    }
}


function filterNotFollowing(followerList, followingList) {
    function notFollowing(x) {
        return ! followingList.includes(x);
    }
    return followerList.filter(notFollowing);
}

// List of people this user is following
async function getFollowingList(userId, option_max_id=null) {
   function url() {
        if(option_max_id !== null) {
            return "https://poshmark.com/api/users/" + userId + "/following?&max_id=" + option_max_id;
        } else {
            return "https://poshmark.com/api/users/" + userId + "/following";
        }
    }

    let method = "GET";

    let response = await fetch(url(), {
        method: method
    });

    if(response.status === 200) {
        let json = await response.json();
        if(json.more !== undefined) {
            let ret = json.data.map(item => item.id).concat(await getFollowingList(userId, json.more.next_max_id));
            return ret;
        } else {
            let ret = json.data.map(item => item.id);
            return ret;
        }
    } else {
        console.log(response.statusText);
        throw new Error("Failed to get following list. responseStatus: " + response.status + " responseText:" + response.statusText);
    }
}

// List of people who are followers of this user
async function getFollowersList(userId, option_max_id=null) {

    function url() {
        if(option_max_id !== null) {
            return "https://poshmark.com/api/users/" + userId + "/followers?&max_id=" + option_max_id;
        } else {
            return "https://poshmark.com/api/users/" + userId + "/followers";
        }
    }

    let method = "GET";

    let response = await fetch(url(), {
        method: method
    });

    if(response.status === 200) {
        let json = await response.json();
        if(json.more !== undefined) {
            let ret = json.data.map(item => item.id).concat(await getFollowersList(userId, json.more.next_max_id));
            return ret;
        } else {
            let ret = json.data.map(item => item.id);
            return ret;
        }
    } else {
        console.log(response.statusText);
        throw new Error("Failed to get followers list. responseStatus: " + response.status + " responseText:" + response.statusText);
    }
}

async function followUser(following) {
    let url = "https://poshmark.com/user/" + following + "/follow_user";
    let method = "POST";
    let response = await fetch(url, {
        method: method
    });
    let json = await response.json();

    let wait = Math.floor(1000.00 * (1.0 + Math.random()));

    await sleep(wait);
    if(json.success === true) {
        return true;
    } else {
        console.log(response.statusText);
        throw new Error("Failed to follow user with id: " + following);
    }

}

