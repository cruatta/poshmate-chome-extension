# Poshmate
## A Poshmark browser extension to automate tedious tasks

**This project is no longer in development and unsupported**

I built this as a present for someone and as a small project to learn a bit of React and JS. They found it very useful for a while, but stopped using Poshmark, so I stopped supporting this extension. Some features might not work, although I imagine this could be a reference project for someone who wants to extend this.

I am open sourcing this in the hopes that my work can be of use to others and I am licensing it accordingly.

## Building
### `npm run build`

Builds the app for production to the `build` folder.<br>
It correctly bundles React in production mode and optimizes the build for the best performance.

The build is minified and the filenames include the hashes.<br>
Your app is ready to be deployed!

See the section about [deployment](https://facebook.github.io/create-react-app/docs/deployment) for more information.

## License
GPL v3


