/* global chrome */ 
import React, { Component } from 'react';
import './journal.min.css';
import './App.css';
import './Loading.css'
import 'chrome-extension-async'

class App extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      loggedIn: false
    };
  }

  async isLoggedIn() {
    let result = await chrome.runtime.sendMessage({action: "is-logged-in"});
    this.setState({loggedIn: result});
  } 

  componentDidMount() {
    this._asyncRequest = this.isLoggedIn().then(result => {
      this._asyncRequest = null;
    });
  }

  componentWillUnmount() {
    if (this._asyncRequest) {
      this._asyncRequest.cancel();
    }
  }

  render() {
    return (
      <div className="App">
        <ul class="list-group">
          <a href="#" class="list-group-item list-group-item-action active">
            <b>Poshmate☆</b>
          </a>
          <ShareCloset loggedIn={this.state.loggedIn}/>
          <FollowUsers loggedIn={this.state.loggedIn}/>
          <LoggedInState loggedIn={this.state.loggedIn}/>
        </ul>
      </div>
    );
  }
}

class LoggedInState extends React.Component {
  constructor(props) {
    super(props);
    this.handleClick = this.handleClick.bind(this);
  }

  async handleClick() {
    await chrome.tabs.create({url: "https://poshmark.com/login"});
  }

  render() {
    if(this.props.loggedIn) {
      return (
        <li class="list-group-item d-flex justify-content-between align-items-center">
        <b>Logged in</b>
        </li>
      )
    }
    else {
      return (
        <li class="list-group-item d-flex justify-content-between align-items-center">
        <button class="btn btn-primary" onClick={this.handleClick}>
            Login
        </button>
        </li>
      )
    }
  }
}

class Loading extends React.Component {
  render() {
    return (
     <span class="badge badge-primary badge-pill">
      <div class="spinner">
        <div class="bounce1"></div>
        <div class="bounce2"></div>
        <div class="bounce3"></div>
      </div>
    </span>
    )
  }
}

class Completed extends React.Component {
  render() {
    return (
      <span class="badge badge-primary badge-pill">
        <div>✓</div>
      </span>
    )
  }
}

class Status extends React.Component {
  render() {
    if(this.props.isLoading) {
        return <Loading />;
    }
    else {
        return <Completed />;
    }
  }
}

class ShareCloset extends React.Component {
    constructor(props) {
      super(props);
      this.state = {
          loading: false
      };

    // This binding is necessary to make `this` work in the callback
    this.handleClick = this.handleClick.bind(this);
  }

  componentDidMount() {
    this._asyncRequest = this.getLoadingState().then(result => {
      this._asyncRequest = null;
    })
  }

  componentWillUnmount() {
    if (this._asyncRequest) {
      this._asyncRequest.cancel();
    }
  }

  async getLoadingState() {
    let state = await chrome.runtime.sendMessage({action: "running-share-closet"});
    if(state === null || state === false){
      this.setState({ loading: false });
    } else {
      this.setState({ loading: true });
    }
   }

  async handleClick() {
    try {
      this.setState({ loading: true });
      await chrome.runtime.sendMessage({action: "share-closet"});
      
      this.setState({ loading: false });
    } catch(error) {
      console.log(error);
    }
  }

  render() {
    if(this.props.loggedIn) {
      return (
        <li class="list-group-item d-flex justify-content-between align-items-center">
            <button class="btn btn-primary" onClick={this.handleClick}>
                Share Closet
            </button>
            <Status isLoading={this.state.loading} />
        </li>
      );
    }
    else {
      return (
        <li class="list-group-item d-flex justify-content-between align-items-center">
            <button class="btn btn-primary disabled">
                Share Closet
            </button>
            <Status isLoading={this.state.loading} />
        </li>
      );
    }
  }
}

class FollowUsers extends React.Component {
  constructor(props) {
    super(props);
      this.state = {
          loading: false
      }
    // This binding is necessary to make `this` work in the callback
    this.handleClick = this.handleClick.bind(this);
  }

  componentDidMount() {
    this._asyncRequest = this.getLoadingState()
  }

  componentWillUnmount() {
    if (this._asyncRequest) {
      this._asyncRequest.cancel();
    }
  }

  async getLoadingState() {
    let state = await chrome.runtime.sendMessage({action: "running-follow-back-followers"});
    if(state === null || state === false){
      this.setState({ loading: false });
    } else {
      this.setState({ loading: true });
    }
   }

  async handleClick() {
    try {
      this.setState({ loading: true });
      await chrome.runtime.sendMessage({action: "follow-back-followers"});
      this.setState({ loading: false });
    } catch(error) {
      console.log(error);
    }
  }

  render() {
    if(this.props.loggedIn) {
      return (
        <li class="list-group-item d-flex justify-content-between align-items-center">
            <button class="btn btn-primary" onClick={this.handleClick}>
            Follow back followers
            </button>
            <Status isLoading={this.state.loading} />
        </li>
      );
    } else {
      return (
        <li class="list-group-item d-flex justify-content-between align-items-center">
            <button class="btn btn-primary disabled">
            Follow back followers
            </button>
            <Status isLoading={this.state.loading} />
        </li>
      ); 
    }
  }
}

export default App;
